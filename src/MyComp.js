import { Component } from 'react';

class MyComp extends Component {
  constructor(props) {
    // console.log("🚀 ~ file: MyComp.js ~ line 5 ~ MyComp ~ constructor ~ props", props)
    super(props)
    this.state = {
      test: 1,
      a: 'test'
    }
    // this.myClick = this.myClick.bind(this)
  }

  myClick = () => {
    this.setState({test: this.state.test + 1})
  }

  componentDidMount() {
    // this.setState({test: this.state.test + 1})
    // this.forceUpdate()
    // console.log('componentDidMount')
  }

  componentWillUnmount() {

  }

  componentDidUpdate(prevProps) {
    // console.log('componentDidUpdate')
    if (prevProps.name !== this.props.name) {
      this.setState({test: this.state.test + 10});
    }
  }

  render() {
    console.log(this.props.id, this.props.name)
    return <div>
      TEST22 {this.state.test} {this.state.a} {this.props.name} {this.props.caption}
      <button onClick={this.myClick}>Test</button>
    </div>
  }
}

export default MyComp;
