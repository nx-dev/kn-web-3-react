import logo from './logo.svg';
import './App.css';
import { Component } from 'react';
import MyComp from './MyComp';

/*
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}
*/

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      test: 1,
      a: 'test',
      items: [
        {
          id: 1,
          name: 'One',
          caption: 'One'
        },
        {
          id: 2,
          name: 'Two',
          caption: 'Two'
        },
      ]
    }
    // this.myClick = this.myClick.bind(this)
  }

  myClick = () => {
    // console.log('111')
    // this.state.test = 2
    this.setState({test: this.state.test + 1})
    this.state.items[1].name = this.state.items[1].name + '!';
    this.setState({items: this.state.items})
  }

  myClick2 = () => {
    this.state.items[1].caption = this.state.items[1].caption + '!';
    this.setState({items: this.state.items})
  }

  componentDidMount() {
    // this.setState({test: this.state.test + 1})
    // this.forceUpdate()
    // console.log('componentDidMount')
  }

  componentWillUnmount() {

  }

  componentDidUpdate() {
    // console.log('componentDidUpdate')
  }

  render() {
    // console.log(this.state.test)
    return <div>
      TEST22 {this.state.test} {this.state.a}
      <button onClick={this.myClick}>Test</button>
      <button onClick={this.myClick2}>Test</button>
      {this.state.items.map((item) => 
        <MyComp key={item.id} id={item.id} name={item.name} caption={item.caption} />
      )}
    </div>
  }
}

export default App;
